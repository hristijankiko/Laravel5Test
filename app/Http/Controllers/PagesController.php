<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    public function about(){

        $people = [
            'John1', 'John2', 'John3'
        ];

        return view('pages.about', compact('people'));
    }
}
