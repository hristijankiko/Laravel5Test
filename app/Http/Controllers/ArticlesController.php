<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Article;
use App\Http\Requests;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ArticlesController extends Controller
{

    public function __construct(){
        $this->middleware('auth', ['only'=>'create']);
    }


    public function index(){

        $articles = Article::latest('published_at')->published()->get();
        $authUser = Auth::user();
        return view('articles.index', compact('articles', 'authUser'));
    }

    public function show($id){
        $article = Article::findOrFail($id);
        $authUser = Auth::user();

        return view('articles.show', compact('article', 'authUser'));
    }

    public function create(){

        $tags = Tag::lists('name', 'id');

        return view('articles.create', compact('tags'));
    }

    public function store(ArticleRequest $request){

        $article = Auth::user()->articles()->create($request->all());



        $article->tags()->attach($request->input('tags'));

        return redirect('articles')->with([
            'flash_message' => 'Your article has been created',
            'flash_message_important' => true
        ]);
    }

    public function edit($id){

        $article = Article::findOrFail($id);

        $tags = Tag::lists('name', 'id');

        return view('articles.edit', compact('article', 'tags') );
    }

    public function update($id, ArticleRequest $request){
        $article = Article::findOrFail($id);



        $article->update($request->all());

        return redirect('articles');
    }
}
