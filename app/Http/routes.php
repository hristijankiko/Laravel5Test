<?php


Route::group(['middleware' => ['web']], function () {
    Route::get('about', 'PagesController@about');


    Route::resource('articles', 'ArticlesController');

    Route::get('foo', ['middleware'=>'manager' ,function(){
        return 'this page can only be viewed by managers';
    }]);

});


Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});
